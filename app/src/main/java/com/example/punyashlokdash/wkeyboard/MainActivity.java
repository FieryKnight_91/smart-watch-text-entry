package com.example.punyashlokdash.wkeyboard;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.wearable.view.WatchViewStub;
import android.text.SpannableStringBuilder;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.PopupWindow;


public class MainActivity extends Activity implements View.OnTouchListener{

    //---------------bcd.xml-------------------------------

    public View.OnTouchListener cancel_button_click_listener1 = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                Vibrator vb = (Vibrator)   getSystemService(Context.VIBRATOR_SERVICE);
                vb.vibrate(100);
                lastDown = System.currentTimeMillis();
            } else if (event.getAction() == MotionEvent.ACTION_UP){
                lastUp = System.currentTimeMillis();
                if((lastUp - lastDown)<350){
                    et.getText().insert(et.getSelectionStart(), "b");
                }
                else{
                    et.getText().insert(et.getSelectionStart(), "B");
                }
                pwindo.dismiss();
            }
            return false;
        }
    };
    public View.OnTouchListener cancel_button_click_listener2 = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                Vibrator vb = (Vibrator)   getSystemService(Context.VIBRATOR_SERVICE);
                vb.vibrate(100);
                lastDown = System.currentTimeMillis();
            } else if (event.getAction() == MotionEvent.ACTION_UP){
                lastUp = System.currentTimeMillis();
                if((lastUp - lastDown)<350){
                    et.getText().insert(et.getSelectionStart(), "c");
                }
                else{
                    et.getText().insert(et.getSelectionStart(), "C");
                }
                pwindo.dismiss();
            }
            return false;
        }
    };
    public View.OnTouchListener cancel_button_click_listener3 = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                Vibrator vb = (Vibrator)   getSystemService(Context.VIBRATOR_SERVICE);
                vb.vibrate(100);
                lastDown = System.currentTimeMillis();
            } else if (event.getAction() == MotionEvent.ACTION_UP){
                lastUp = System.currentTimeMillis();
                if((lastUp - lastDown)<350){
                    et.getText().insert(et.getSelectionStart(), "d");
                }
                else{
                    et.getText().insert(et.getSelectionStart(), "D");
                }
                pwindo.dismiss();
            }
            return false;
        }
    };
    public View.OnTouchListener cancel_button_click_listener4 = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                Vibrator vb = (Vibrator)   getSystemService(Context.VIBRATOR_SERVICE);
                vb.vibrate(100);
                lastDown = System.currentTimeMillis();
            } else if (event.getAction() == MotionEvent.ACTION_UP){
                lastUp = System.currentTimeMillis();
                if((lastUp - lastDown)<350){
                    et.getText().insert(et.getSelectionStart(), "f");
                }
                else{
                    et.getText().insert(et.getSelectionStart(), "F");
                }
                pwindo.dismiss();
            }
            return false;
        }
    };
    public View.OnTouchListener cancel_button_click_listener5 = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                Vibrator vb = (Vibrator)   getSystemService(Context.VIBRATOR_SERVICE);
                vb.vibrate(100);
                lastDown = System.currentTimeMillis();
            } else if (event.getAction() == MotionEvent.ACTION_UP){
                lastUp = System.currentTimeMillis();
                if((lastUp - lastDown)<350){
                    et.getText().insert(et.getSelectionStart(), "g");
                }
                else{
                    et.getText().insert(et.getSelectionStart(), "G");
                }
                pwindo.dismiss();
            }
            return false;
        }
    };
    public View.OnTouchListener cancel_button_click_listener6 = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                Vibrator vb = (Vibrator)   getSystemService(Context.VIBRATOR_SERVICE);
                vb.vibrate(100);
                lastDown = System.currentTimeMillis();
            } else if (event.getAction() == MotionEvent.ACTION_UP){
                lastUp = System.currentTimeMillis();
                if((lastUp - lastDown)<350){
                    et.getText().insert(et.getSelectionStart(), "h");
                }
                else{
                    et.getText().insert(et.getSelectionStart(), "H");
                }
                pwindo.dismiss();
            }
            return false;
        }
    };
    public View.OnTouchListener cancel_button_click_listener7 = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                Vibrator vb = (Vibrator)   getSystemService(Context.VIBRATOR_SERVICE);
                vb.vibrate(100);
                lastDown = System.currentTimeMillis();
            } else if (event.getAction() == MotionEvent.ACTION_UP){
                lastUp = System.currentTimeMillis();
                if((lastUp - lastDown)<350){
                    et.getText().insert(et.getSelectionStart(), "j");
                }
                else{
                    et.getText().insert(et.getSelectionStart(), "J");
                }
                pwindo.dismiss();
            }
            return false;
        }
    };
    public View.OnTouchListener cancel_button_click_listener8 = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                Vibrator vb = (Vibrator)   getSystemService(Context.VIBRATOR_SERVICE);
                vb.vibrate(100);
                lastDown = System.currentTimeMillis();
            } else if (event.getAction() == MotionEvent.ACTION_UP){
                lastUp = System.currentTimeMillis();
                if((lastUp - lastDown)<350){
                    et.getText().insert(et.getSelectionStart(), "k");
                }
                else{
                    et.getText().insert(et.getSelectionStart(), "K");
                }
                pwindo.dismiss();
            }
            return false;
        }
    };
   public View.OnTouchListener cancel_button_click_listener9 = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                Vibrator vb = (Vibrator)   getSystemService(Context.VIBRATOR_SERVICE);
                vb.vibrate(100);
                lastDown = System.currentTimeMillis();
            } else if (event.getAction() == MotionEvent.ACTION_UP){
                lastUp = System.currentTimeMillis();
                if((lastUp - lastDown)<350){
                    et.getText().insert(et.getSelectionStart(), "l");
                }
                else{
                    et.getText().insert(et.getSelectionStart(), "L");
                }
                pwindo.dismiss();
            }
            return false;
        }
    };

    public View.OnTouchListener cancel_button_click_listener10 = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                Vibrator vb = (Vibrator)   getSystemService(Context.VIBRATOR_SERVICE);
                vb.vibrate(100);
                pwindo.dismiss();
            }
            return false;
        }
    };


    //-----------------------------for mpq.xml----------------------------------------------------------------
public View.OnTouchListener cancel_button_1 = new View.OnTouchListener() {

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            Vibrator vb = (Vibrator)   getSystemService(Context.VIBRATOR_SERVICE);
            vb.vibrate(100);
            lastDown = System.currentTimeMillis();
        } else if (event.getAction() == MotionEvent.ACTION_UP){
            lastUp = System.currentTimeMillis();
            if((lastUp - lastDown)<350){
                et.getText().insert(et.getSelectionStart(), "m");
            }
            else{
                et.getText().insert(et.getSelectionStart(), "M");
            }
            pwindo.dismiss();
        }
        return false;
    }
};

    public View.OnTouchListener cancel_button_2 = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                Vibrator vb = (Vibrator)   getSystemService(Context.VIBRATOR_SERVICE);
                vb.vibrate(100);
                lastDown = System.currentTimeMillis();
            } else if (event.getAction() == MotionEvent.ACTION_UP){
                lastUp = System.currentTimeMillis();
                if((lastUp - lastDown)<350){
                    et.getText().insert(et.getSelectionStart(), "p");
                }
                else{
                    et.getText().insert(et.getSelectionStart(), "P");
                }
                pwindo.dismiss();
            }
            return false;
        }
    };

    public View.OnTouchListener cancel_button_3 = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                Vibrator vb = (Vibrator)   getSystemService(Context.VIBRATOR_SERVICE);
                vb.vibrate(100);
                lastDown = System.currentTimeMillis();
            } else if (event.getAction() == MotionEvent.ACTION_UP){
                lastUp = System.currentTimeMillis();
                if((lastUp - lastDown)<350){
                    et.getText().insert(et.getSelectionStart(), "q");
                }
                else{
                    et.getText().insert(et.getSelectionStart(), "Q");
                }
                pwindo.dismiss();
            }
            return false;
        }
    };

    public View.OnTouchListener cancel_button_4 = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                Vibrator vb = (Vibrator)   getSystemService(Context.VIBRATOR_SERVICE);
                vb.vibrate(100);
                lastDown = System.currentTimeMillis();
            } else if (event.getAction() == MotionEvent.ACTION_UP){
                lastUp = System.currentTimeMillis();
                if((lastUp - lastDown)<350){
                    et.getText().insert(et.getSelectionStart(), "u");
                }
                else{
                    et.getText().insert(et.getSelectionStart(), "U");
                }
                pwindo.dismiss();
            }
            return false;
        }
    };

    public View.OnTouchListener cancel_button_5 = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                Vibrator vb = (Vibrator)   getSystemService(Context.VIBRATOR_SERVICE);
                vb.vibrate(100);
                lastDown = System.currentTimeMillis();
            } else if (event.getAction() == MotionEvent.ACTION_UP){
                lastUp = System.currentTimeMillis();
                if((lastUp - lastDown)<350){
                    et.getText().insert(et.getSelectionStart(), "v");
                }
                else{
                    et.getText().insert(et.getSelectionStart(), "V");
                }
                pwindo.dismiss();
            }
            return false;
        }
    };

    public View.OnTouchListener cancel_button_6 = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                Vibrator vb = (Vibrator)   getSystemService(Context.VIBRATOR_SERVICE);
                vb.vibrate(100);
                lastDown = System.currentTimeMillis();
            } else if (event.getAction() == MotionEvent.ACTION_UP){
                lastUp = System.currentTimeMillis();
                if((lastUp - lastDown)<350){
                    et.getText().insert(et.getSelectionStart(), "w");
                }
                else{
                    et.getText().insert(et.getSelectionStart(), "W");
                }
                pwindo.dismiss();
            }
            return false;
        }
    };

    public View.OnTouchListener cancel_button_7 = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                Vibrator vb = (Vibrator)   getSystemService(Context.VIBRATOR_SERVICE);
                vb.vibrate(100);
                lastDown = System.currentTimeMillis();
            } else if (event.getAction() == MotionEvent.ACTION_UP){
                lastUp = System.currentTimeMillis();
                if((lastUp - lastDown)<350){
                    et.getText().insert(et.getSelectionStart(), "x");
                }
                else{
                    et.getText().insert(et.getSelectionStart(), "X");
                }
                pwindo.dismiss();
            }
            return false;
        }
    };

    public View.OnTouchListener cancel_button_8 = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                Vibrator vb = (Vibrator)   getSystemService(Context.VIBRATOR_SERVICE);
                vb.vibrate(100);
                lastDown = System.currentTimeMillis();
            } else if (event.getAction() == MotionEvent.ACTION_UP){
                lastUp = System.currentTimeMillis();
                if((lastUp - lastDown)<350){
                    et.getText().insert(et.getSelectionStart(), "y");
                }
                else{
                    et.getText().insert(et.getSelectionStart(), "Y");
                }
                pwindo.dismiss();
            }
            return false;
        }
    };

    public View.OnTouchListener cancel_button_9 = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                Vibrator vb = (Vibrator)   getSystemService(Context.VIBRATOR_SERVICE);
                vb.vibrate(100);
                lastDown = System.currentTimeMillis();
            } else if (event.getAction() == MotionEvent.ACTION_UP){
                lastUp = System.currentTimeMillis();
                if((lastUp - lastDown)<350){
                    et.getText().insert(et.getSelectionStart(), "z");
                }
                else{
                    et.getText().insert(et.getSelectionStart(), "Z");
                }
                pwindo.dismiss();
            }
            return false;
        }
    };

    public View.OnTouchListener cancel_button_10 = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                Vibrator vb = (Vibrator)   getSystemService(Context.VIBRATOR_SERVICE);
                vb.vibrate(100);
                pwindo.dismiss();
            }
            return false;
        }
    };

//-----------------------------------number.xml-------------------------------------------------------------

    public View.OnTouchListener cancel_number_button_2 = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
             if (event.getAction() == MotionEvent.ACTION_DOWN){
                 Vibrator vb = (Vibrator)   getSystemService(Context.VIBRATOR_SERVICE);
                 vb.vibrate(100);
                 et.getText().insert(et.getSelectionStart(), "0");
             }
            return false;
        }
    };


    public View.OnTouchListener cancel_number_button_3 = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN){
                Vibrator vb = (Vibrator)   getSystemService(Context.VIBRATOR_SERVICE);
                vb.vibrate(100);
                et.getText().insert(et.getSelectionStart(), "1");
            }
            return false;
        }
    };

    public View.OnTouchListener cancel_number_button_4 = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN){
                Vibrator vb = (Vibrator)   getSystemService(Context.VIBRATOR_SERVICE);
                vb.vibrate(100);
                et.getText().insert(et.getSelectionStart(), "2");
            }
            return false;
        }
    };

    public View.OnTouchListener cancel_number_button_5 = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN){
                Vibrator vb = (Vibrator)   getSystemService(Context.VIBRATOR_SERVICE);
                vb.vibrate(100);
                et.getText().insert(et.getSelectionStart(), "3");
            }
            return false;
        }
    };

    public View.OnTouchListener cancel_number_button_6 = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN){
                Vibrator vb = (Vibrator)   getSystemService(Context.VIBRATOR_SERVICE);
                vb.vibrate(100);
                et.getText().insert(et.getSelectionStart(), "4");
            }
            return false;
        }
    };

    public View.OnTouchListener cancel_number_button_7 = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN){
                Vibrator vb = (Vibrator)   getSystemService(Context.VIBRATOR_SERVICE);
                vb.vibrate(100);
                et.getText().insert(et.getSelectionStart(), "5");
            }
            return false;
        }
    };

    public View.OnTouchListener cancel_number_button_8 = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN){
                Vibrator vb = (Vibrator)   getSystemService(Context.VIBRATOR_SERVICE);
                vb.vibrate(100);
                et.getText().insert(et.getSelectionStart(), "6");
            }
            return false;
        }
    };

    public View.OnTouchListener cancel_number_button_9 = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN){
                Vibrator vb = (Vibrator)   getSystemService(Context.VIBRATOR_SERVICE);
                vb.vibrate(100);
                et.getText().insert(et.getSelectionStart(), "7");
            }
            return false;
        }
    };

    public View.OnTouchListener cancel_number_button_10 = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN){
                Vibrator vb = (Vibrator)   getSystemService(Context.VIBRATOR_SERVICE);
                vb.vibrate(100);
                et.getText().insert(et.getSelectionStart(), "8");
            }
            return false;
        }
    };

    public View.OnTouchListener cancel_number_button_11 = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN){
                Vibrator vb = (Vibrator)   getSystemService(Context.VIBRATOR_SERVICE);
                vb.vibrate(100);
                et.getText().insert(et.getSelectionStart(), "9");
            }
            return false;
        }
    };

    public View.OnTouchListener cancel_number_button_12 = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                Vibrator vb = (Vibrator)   getSystemService(Context.VIBRATOR_SERVICE);
                vb.vibrate(100);
                pwindo.dismiss();
            }
            return false;
        }
    };

    //-------------------------------symbols--------------------------------------------------------------------------
        public View.OnTouchListener cancel_symbol_button_1 = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN){
                Vibrator vb = (Vibrator)   getSystemService(Context.VIBRATOR_SERVICE);
                vb.vibrate(100);
                et.getText().insert(et.getSelectionStart(), "!");
                pwindo.dismiss();
            }
            return false;
        }
    };

    public View.OnTouchListener cancel_symbol_button_2 = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN){
                Vibrator vb = (Vibrator)   getSystemService(Context.VIBRATOR_SERVICE);
                vb.vibrate(100);
                et.getText().insert(et.getSelectionStart(), ".");
                pwindo.dismiss();
            }
            return false;
        }
    };

    public View.OnTouchListener cancel_symbol_button_3 = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN){
                Vibrator vb = (Vibrator)   getSystemService(Context.VIBRATOR_SERVICE);
                vb.vibrate(100);
                et.getText().insert(et.getSelectionStart(), ",");
                pwindo.dismiss();
            }
            return false;
        }
    };

    public View.OnTouchListener cancel_symbol_button_4 = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                Vibrator vb = (Vibrator)   getSystemService(Context.VIBRATOR_SERVICE);
                vb.vibrate(100);
                lastDown = System.currentTimeMillis();
            } else if (event.getAction() == MotionEvent.ACTION_UP){
                lastUp = System.currentTimeMillis();
                if((lastUp - lastDown)<350){
                    et.getText().insert(et.getSelectionStart(), "(");
                }
                else{
                    et.getText().insert(et.getSelectionStart(), ")");
                }
                pwindo.dismiss();
            }
            return false;
        }
    };

    public View.OnTouchListener cancel_symbol_button_5 = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                Vibrator vb = (Vibrator)   getSystemService(Context.VIBRATOR_SERVICE);
                vb.vibrate(100);
                lastDown = System.currentTimeMillis();
            } else if (event.getAction() == MotionEvent.ACTION_UP){
                lastUp = System.currentTimeMillis();
                if((lastUp - lastDown)<350){
                    et.getText().insert(et.getSelectionStart(), "'");
                }
                else{
                    et.getText().insert(et.getSelectionStart(), "-");
                }
                pwindo.dismiss();
            }
            return false;
        }
    };

    public View.OnTouchListener cancel_symbol_button_6 = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                Vibrator vb = (Vibrator)   getSystemService(Context.VIBRATOR_SERVICE);
                vb.vibrate(100);
                lastDown = System.currentTimeMillis();
            } else if (event.getAction() == MotionEvent.ACTION_UP){
                lastUp = System.currentTimeMillis();
                if((lastUp - lastDown)<350){
                    et.getText().insert(et.getSelectionStart(), "_");
                }
                else{
                    et.getText().insert(et.getSelectionStart(), "+");
                }
                pwindo.dismiss();
            }
            return false;
        }
    };

    public View.OnTouchListener cancel_symbol_button_7 = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                Vibrator vb = (Vibrator)   getSystemService(Context.VIBRATOR_SERVICE);
                vb.vibrate(100);
                lastDown = System.currentTimeMillis();
            } else if (event.getAction() == MotionEvent.ACTION_UP){
                lastUp = System.currentTimeMillis();
                if((lastUp - lastDown)<350){
                    et.getText().insert(et.getSelectionStart(), ";");
                }
                else{
                    et.getText().insert(et.getSelectionStart(), "@");
                }
                pwindo.dismiss();
            }
            return false;
        }
    };

    public View.OnTouchListener cancel_symbol_button_8 = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN){
                Vibrator vb = (Vibrator)   getSystemService(Context.VIBRATOR_SERVICE);
                vb.vibrate(100);
                et.getText().insert(et.getSelectionStart(), "#");
                pwindo.dismiss();
            }
            return false;
        }
    };

    public View.OnTouchListener cancel_symbol_button_9 = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN){
                Vibrator vb = (Vibrator)   getSystemService(Context.VIBRATOR_SERVICE);
                vb.vibrate(100);
                et.getText().insert(et.getSelectionStart(), "?");
                pwindo.dismiss();
            }
            return false;
        }
    };

    public View.OnTouchListener cancel_symbol_button_10 = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN){
                Vibrator vb = (Vibrator)   getSystemService(Context.VIBRATOR_SERVICE);
                vb.vibrate(100);
                et.getText().insert(et.getSelectionStart(), "/");
                pwindo.dismiss();
            }
            return false;
        }
    };

    public View.OnTouchListener cancel_symbol_button_11 = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN){
                Vibrator vb = (Vibrator)   getSystemService(Context.VIBRATOR_SERVICE);
                vb.vibrate(100);
                et.getText().insert(et.getSelectionStart(), ":");
                pwindo.dismiss();
            }
            return false;
        }
    };

    public View.OnTouchListener cancel_symbol_button_12 = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN){
                Vibrator vb = (Vibrator)   getSystemService(Context.VIBRATOR_SERVICE);
                vb.vibrate(100);
                pwindo.dismiss();
            }
            return false;
        }
    };

    //-------------------------------------------------------------------

    private EditText et;
    Button bt1, bt2, bt3, bt4, bt5, bt6, bt7, bt8, bt9, bt10, bt11, bt12,btn1,btn2,btn3,btn4,btn5,btn6,btn7,btn8,btn9;
    Button btm1,btm2,btm3,btm4,btm5,btm6,btm7,btm8,btm9;
    Button n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,s1,s2,s3,s4,s5,s6,s7,s8,s9,s10,s11;
    ImageButton btm10,btn10,n12,s12;
    PopupWindow pwindo;
    double lastDown = 0,lastUp=0;
    private boolean touchStarted = false;
    private int dx, dy, ux, uy;

    float touchOrientation,touchMajor,touchMinor,pressure,histOrientation; // Added to check if orientation of finger is captured

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final WatchViewStub stub = (WatchViewStub) findViewById(R.id.watch_view_stub);
        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                et = (EditText) stub.findViewById(R.id.editText);

                bt1= (Button) stub.findViewById(R.id.b1);
                bt1.setOnTouchListener(MainActivity.this);

                bt2 = (Button) stub.findViewById(R.id.b2);
                bt2.setOnTouchListener(MainActivity.this);

                bt3 = (Button) stub.findViewById(R.id.button);
                bt3.setOnTouchListener(MainActivity.this);


                bt4 = (Button) stub.findViewById(R.id.button2);
                bt4.setOnTouchListener(MainActivity.this);

                bt5 = (Button) stub.findViewById(R.id.button3);
                bt5.setOnTouchListener(MainActivity.this);

                bt6 = (Button) stub.findViewById(R.id.button4);
                bt6.setOnTouchListener(MainActivity.this);

                bt7 = (Button) stub.findViewById(R.id.button5);
                bt7.setOnTouchListener(MainActivity.this);

                bt8 = (Button) stub.findViewById(R.id.button6);
                bt8.setOnTouchListener(MainActivity.this);

                bt9 = (Button) stub.findViewById(R.id.button7);
                bt9.setOnTouchListener(MainActivity.this);

                bt10 = (Button) stub.findViewById(R.id.button8);
                bt10.setOnTouchListener(MainActivity.this);

                bt11 = (Button) stub.findViewById(R.id.button9);
                bt11.setOnTouchListener(MainActivity.this);

                bt12 = (Button) stub.findViewById(R.id.button10);
                bt12.setOnTouchListener(MainActivity.this);
            }
        });

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        try {
            switch (v.getId())
            {
                case R.id.button:
                    if (event.getAction() == MotionEvent.ACTION_DOWN)
                    {
                        Vibrator vb = (Vibrator)   getSystemService(Context.VIBRATOR_SERVICE);
                        vb.vibrate(100);
                        lastDown = System.currentTimeMillis();
                    }
                    else if(event.getAction() == MotionEvent.ACTION_MOVE)   //ADDED to Intercept touch behaviour
                    {
                        touchOrientation = event.getOrientation();
                        System.out.println("Touch orientation = "+touchOrientation);
                        touchMajor = event.getTouchMajor();
                        System.out.println("Touch Major = "+touchMajor);
                        touchMinor = event.getTouchMinor();
                        System.out.println("Touch Minor = "+touchMinor);
                        pressure = event.getPressure();
                        System.out.println("Touch pressure = "+pressure);
                        histOrientation = event.getHistoricalOrientation(1);
                        System.out.println("Hist Orientation = "+histOrientation);

                    }
                    else if (event.getAction() == MotionEvent.ACTION_UP)
                    {
                        lastUp = System.currentTimeMillis();
                        if (((lastUp - lastDown) < 350)) {
                            et.getText().insert(et.getSelectionStart(), "e");
                            //String st = et.getText().toString();
                            //et.setText(st + "e");
                            //et.setSelection(et.getText().length());
                        }else {
                            et.getText().insert(et.getSelectionStart(), "E");
                        }
                    }
                    break;
                case R.id.button2:
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        Vibrator vb = (Vibrator)  getSystemService(Context.VIBRATOR_SERVICE);
                        vb.vibrate(100);
                        lastDown = System.currentTimeMillis();
                    }
                    else if(event.getAction() == MotionEvent.ACTION_MOVE)              //ADDED to Intercept touch behaviour
                    {
                        touchOrientation = event.getOrientation();
                        System.out.println("Touch orientation = "+touchOrientation);
                        touchMajor = event.getTouchMajor();
                        System.out.println("Touch Major = "+touchMajor);
                        touchMinor = event.getTouchMinor();
                        System.out.println("Touch Minor = "+touchMinor);
                        pressure = event.getPressure();
                        System.out.println("Touch pressure = "+pressure);
                        histOrientation = event.getHistoricalOrientation(1);
                        System.out.println("Hist Orientation = "+histOrientation);

                    }
                    else if (event.getAction() == MotionEvent.ACTION_UP) {
                        lastUp = System.currentTimeMillis();
                        if ((lastUp - lastDown) < 350) {
                            et.getText().insert(et.getSelectionStart(), "a");
                        } else {
                            et.getText().insert(et.getSelectionStart(), "A");
                        }
                    }
                    break;
                case R.id.button3:
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        Vibrator vb = (Vibrator)   getSystemService(Context.VIBRATOR_SERVICE);
                        vb.vibrate(100);
                        lastDown = System.currentTimeMillis();
                    } else if (event.getAction() == MotionEvent.ACTION_UP) {
                        lastUp = System.currentTimeMillis();
                        if ((lastUp - lastDown) < 350) {
                            et.getText().insert(et.getSelectionStart(), "o");
                        } else {
                            et.getText().insert(et.getSelectionStart(), "O");
                        }
                    }
                    break;
                case R.id.button4:
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        Vibrator vb = (Vibrator)   getSystemService(Context.VIBRATOR_SERVICE);
                        vb.vibrate(100);
                        lastDown = System.currentTimeMillis();
                    } else if (event.getAction() == MotionEvent.ACTION_UP) {
                        lastUp = System.currentTimeMillis();
                        if ((lastUp - lastDown) < 350) {
                            et.getText().insert(et.getSelectionStart(), "t");
                        } else {
                            et.getText().insert(et.getSelectionStart(), "T");
                        }
                    }
                    break;
                case R.id.button5:
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        Vibrator vb = (Vibrator)   getSystemService(Context.VIBRATOR_SERVICE);
                        vb.vibrate(100);
                        lastDown = System.currentTimeMillis();
                    } else if (event.getAction() == MotionEvent.ACTION_UP) {
                        lastUp = System.currentTimeMillis();
                        if ((lastUp - lastDown) < 350) {
                            et.getText().insert(et.getSelectionStart(), "i");
                        } else {
                            et.getText().insert(et.getSelectionStart(), "I");
                        }
                    }
                    break;
                case R.id.button6:
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        Vibrator vb = (Vibrator)   getSystemService(Context.VIBRATOR_SERVICE);
                        vb.vibrate(100);
                        lastDown = System.currentTimeMillis();
                    } else if (event.getAction() == MotionEvent.ACTION_UP) {
                        lastUp = System.currentTimeMillis();
                        if ((lastUp - lastDown) < 350) {
                            et.getText().insert(et.getSelectionStart(), "n");
                        } else {
                            et.getText().insert(et.getSelectionStart(), "N");
                        }
                    }
                    break;
                case R.id.button7:
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        Vibrator vb = (Vibrator)   getSystemService(Context.VIBRATOR_SERVICE);
                        vb.vibrate(100);
                        lastDown = System.currentTimeMillis();
                    } else if (event.getAction() == MotionEvent.ACTION_UP) {
                        lastUp = System.currentTimeMillis();
                        if ((lastUp - lastDown) < 350) {
                            et.getText().insert(et.getSelectionStart(), "s");
                        } else {
                            et.getText().insert(et.getSelectionStart(), "S");
                        }
                    }
                    break;
                case R.id.button8:
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        Vibrator vb = (Vibrator)   getSystemService(Context.VIBRATOR_SERVICE);
                        vb.vibrate(100);
                        lastDown = System.currentTimeMillis();
                    } else if (event.getAction() == MotionEvent.ACTION_UP) {
                        lastUp = System.currentTimeMillis();
                        if ((lastUp - lastDown) < 350) {
                            et.getText().insert(et.getSelectionStart(), "r");
                        } else {
                            et.getText().insert(et.getSelectionStart(), "R");
                        }
                    }
                    break;
                case R.id.b1:
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        Vibrator vb = (Vibrator)   getSystemService(Context.VIBRATOR_SERVICE);
                        vb.vibrate(100);
                        dx = (int) event.getX();
                        dy = (int) event.getY();
                        touchStarted = true;
                    }
                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        ux = (int) event.getX();
                        uy = (int) event.getY();
                        int x = Math.abs(ux - dx);
                        int y = Math.abs(uy - dy);
                        if (y > x) {
                            et.getText().insert(et.getSelectionStart(), " ");
                        }
                        if(x>y){
                            //getting cursor position
                            int end = et.getSelectionEnd();
                            //getting the selected Text
                            SpannableStringBuilder selectedStr=new SpannableStringBuilder(et.getText());
                            //replacing the selected text with empty String and setting it to EditText
                            selectedStr.replace(end-1, end, "");
                            et.setText(selectedStr);
                            et.setSelection(et.getText().length());

                            /*String str = et.getText().toString().trim();
                            if(str.length()!=0){
                            str  = str.substring( 0, str.length() - 1 );
                            et.setText ( str );
                            et.setSelection(et.getText().length());*/

                            }
                        if(x==0 && y==0){
                            initiatePopupWindow_2();
                        }
                    }
                    break;
                case R.id.b2:
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        Vibrator vb = (Vibrator)   getSystemService(Context.VIBRATOR_SERVICE);
                        vb.vibrate(100);
                        dx = (int) event.getX();
                        dy = (int) event.getY();
                        touchStarted = true;
                    }
                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        ux = (int) event.getX();
                        uy = (int) event.getY();
                        int x = Math.abs(ux - dx);
                        int y = Math.abs(uy - dy);
                        if (y > x) {
                            et.getText().insert(et.getSelectionStart(), " ");
                        }
                        if(x>y){
                            int end = et.getSelectionEnd();
                            SpannableStringBuilder selectedStr=new SpannableStringBuilder(et.getText());
                            selectedStr.replace(end-1, end, "");
                            et.setText(selectedStr);
                            et.setSelection(et.getText().length());
                        }
                        if(x==0 && y==0){
                            initiatePopupWindow_3();
                        }
                    }
                    break;
                case R.id.button9:
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        Vibrator vb = (Vibrator)   getSystemService(Context.VIBRATOR_SERVICE);
                        vb.vibrate(100);
                        initiatePopupWindow_1();
                    }
                    break;
                case R.id.button10:
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        Vibrator vb = (Vibrator)   getSystemService(Context.VIBRATOR_SERVICE);
                        vb.vibrate(100);
                        initiatePopupWindow_4();
                    }
                    break;
            }
        }catch (Exception e){

        }
        return false;
    }


    private void initiatePopupWindow_1() {
        LayoutInflater inflater = (LayoutInflater) MainActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.number, (android.view.ViewGroup) findViewById(R.id.number_element));
        pwindo = new PopupWindow(layout, 400, 400, true);
        pwindo.showAtLocation(layout, Gravity.CENTER,0,0);

        n2 = (Button) layout.findViewById(R.id.b1);
        n2.setOnTouchListener(cancel_number_button_2);

        n3 = (Button) layout.findViewById(R.id.b2);
        n3.setOnTouchListener(cancel_number_button_3);

        n4 = (Button) layout.findViewById(R.id.button);
        n4.setOnTouchListener(cancel_number_button_4);

        n5 = (Button) layout.findViewById(R.id.button4);
        n5.setOnTouchListener(cancel_number_button_5);

        n6 = (Button) layout.findViewById(R.id.button2);
        n6.setOnTouchListener(cancel_number_button_6);

        n7 = (Button) layout.findViewById(R.id.button3);
        n7.setOnTouchListener(cancel_number_button_7);

        n8 = (Button) layout.findViewById(R.id.button5);
        n8.setOnTouchListener(cancel_number_button_8);

        n9 = (Button) layout.findViewById(R.id.button6);
        n9.setOnTouchListener(cancel_number_button_9);

        n10 = (Button) layout.findViewById(R.id.button7);
        n10.setOnTouchListener(cancel_number_button_10);

        n11 = (Button) layout.findViewById(R.id.button8);
        n11.setOnTouchListener(cancel_number_button_11);

        n12 = (ImageButton)layout.findViewById(R.id.imageButton3);
        n12.setOnTouchListener(cancel_number_button_12);
    }

    private void initiatePopupWindow_2() {
        try {
            LayoutInflater inflater = (LayoutInflater) MainActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = inflater.inflate(R.layout.bcd, (android.view.ViewGroup) findViewById(R.id.popup_element));
            pwindo = new PopupWindow(layout, 400, 400, true);
            pwindo.showAtLocation(layout, Gravity.HORIZONTAL_GRAVITY_MASK,0,0);

            btn1 = (Button) layout.findViewById(R.id.button9);
            btn1.setOnTouchListener(cancel_button_click_listener1);

            btn2 = (Button) layout.findViewById(R.id.b1);
            btn2.setOnTouchListener(cancel_button_click_listener2);

            btn3 = (Button) layout.findViewById(R.id.b2);
            btn3.setOnTouchListener(cancel_button_click_listener3);

            btn4 = (Button) layout.findViewById(R.id.button);
            btn4.setOnTouchListener(cancel_button_click_listener4);

            btn5 = (Button) layout.findViewById(R.id.button4);
            btn5.setOnTouchListener(cancel_button_click_listener5);

            btn6 = (Button) layout.findViewById(R.id.button2);
            btn6.setOnTouchListener(cancel_button_click_listener6);

            btn7 = (Button) layout.findViewById(R.id.button5);
            btn7.setOnTouchListener(cancel_button_click_listener7);

            btn8 = (Button) layout.findViewById(R.id.button6);
            btn8.setOnTouchListener(cancel_button_click_listener8);

            btn9 = (Button) layout.findViewById(R.id.button7);
            btn9.setOnTouchListener(cancel_button_click_listener9);

            btn10 = (ImageButton) layout.findViewById(R.id.imageButton);
            btn10.setOnTouchListener(cancel_button_click_listener10);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void initiatePopupWindow_3() {
        LayoutInflater inflater = (LayoutInflater) MainActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.mpq, (android.view.ViewGroup) findViewById(R.id.mpq_element));
        pwindo = new PopupWindow(layout, 400, 400, true);
        pwindo.showAtLocation(layout, Gravity.CENTER,0,0);

        btm1 = (Button) layout.findViewById(R.id.b1);
        btm1.setOnTouchListener(cancel_button_1);

        btm2 = (Button) layout.findViewById(R.id.b2);
        btm2.setOnTouchListener(cancel_button_2);

        btm3 = (Button) layout.findViewById(R.id.button10);
        btm3.setOnTouchListener(cancel_button_3);

        btm4 = (Button) layout.findViewById(R.id.button4);
        btm4.setOnTouchListener(cancel_button_4);

        btm5 = (Button) layout.findViewById(R.id.button2);
        btm5.setOnTouchListener(cancel_button_5);

        btm6 = (Button) layout.findViewById(R.id.button3);
        btm6.setOnTouchListener(cancel_button_6);

        btm7 = (Button) layout.findViewById(R.id.button6);
        btm7.setOnTouchListener(cancel_button_7);

        btm8 = (Button) layout.findViewById(R.id.button7);
        btm8.setOnTouchListener(cancel_button_8);

        btm9 = (Button) layout.findViewById(R.id.button8);
        btm9.setOnTouchListener(cancel_button_9);

        btm10 = (ImageButton) layout.findViewById(R.id.imageButton2);
        btm10.setOnTouchListener(cancel_button_10);

    }

    private void initiatePopupWindow_4() {
        LayoutInflater inflater = (LayoutInflater) MainActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.symbol, (android.view.ViewGroup) findViewById(R.id.symbol_element));
        pwindo = new PopupWindow(layout, 400, 400, true);
        pwindo.showAtLocation(layout, Gravity.CENTER,0,0);

        s1 = (Button) layout.findViewById(R.id.button9);
        s1.setOnTouchListener(cancel_symbol_button_1);

        s2 = (Button) layout.findViewById(R.id.b1);
        s2.setOnTouchListener(cancel_symbol_button_2);

        s3 = (Button) layout.findViewById(R.id.b2);
        s3.setOnTouchListener(cancel_symbol_button_3);

        s4 = (Button) layout.findViewById(R.id.button);
        s4.setOnTouchListener(cancel_symbol_button_4);

        s5 = (Button) layout.findViewById(R.id.button4);
        s5.setOnTouchListener(cancel_symbol_button_5);

        s6 = (Button) layout.findViewById(R.id.button2);
        s6.setOnTouchListener(cancel_symbol_button_6);

        s7 = (Button) layout.findViewById(R.id.button3);
        s7.setOnTouchListener(cancel_symbol_button_7);

        s8 = (Button) layout.findViewById(R.id.button5);
        s8.setOnTouchListener(cancel_symbol_button_8);

        s9 = (Button) layout.findViewById(R.id.button6);
        s9.setOnTouchListener(cancel_symbol_button_9);

        s10 = (Button) layout.findViewById(R.id.button7);
        s10.setOnTouchListener(cancel_symbol_button_10);

        s11 = (Button) layout.findViewById(R.id.button8);
        s11.setOnTouchListener(cancel_symbol_button_11);

        s12 = (ImageButton) layout.findViewById(R.id.imageButton4);
        s12.setOnTouchListener(cancel_symbol_button_12);
    }
}
